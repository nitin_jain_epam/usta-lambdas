import json
import os
import boto3
import pymysql.cursors
import sys
import csv
from datetime import datetime
from datetime import date


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('datalake_feeds_last_execution_time')

def get_secret():

    secret_name = os.environ['secret_name']
    region_name = os.environ['secret_region']

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    print ("Trying to get secret: %s\n" % secret_name)
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )

    secret = get_secret_value_response['SecretString']
    return (json.loads(secret))


def lambda_handler(event, context):

    json_data = get_secret()
    SCHOOL_PE_PROGRAMS_QUERY = 'select SCHOOL_PE_PROGRAM_ID, START_DATE, END_DATE, PARTICIPANT_COUNT, PROVIDER_UAID, PARTNER_UAID, IS_RENEWED, IS_DELETED, CREATED_AT, MODIFIED_AT, PROGRAM_SHIPPING_ADDRESS_ID, SCHOOL_TENNIS_UNIT_DURATION_ID, spp.SOURCE_SYSTEM_ID, SCHOOL_PE_ROLE_ID, SCHOOL_REGISTRY_ID, ss.VALUE as "SOURCE_SYSTEM_NAME" from SCHOOL_PE_PROGRAM spp left join SOURCE_SYSTEM ss ON spp.SOURCE_SYSTEM_ID = ss.SOURCE_SYSTEM_ID WHERE spp.MODIFIED_AT >=%s and spp.MODIFIED_AT < %s;'
    SCHOOL_PE_PROGRAM_SCHOOL_SEASONS_QUERY = 'select spss.SCHOOL_PE_PROGRAM_ID, spss.SCHOOL_SEASON_ID, ss.VALUE as "SCHOOL_SEASON_NAME" from SCHOOL_PE_PROGRAM_SCHOOL_SEASONS spss LEFT JOIN SCHOOL_SEASON ss ON spss.SCHOOL_SEASON_ID = ss.SCHOOL_SEASON_ID where ss.MODIFIED_AT >=%s and ss.MODIFIED_AT < %s'
    SCHOOL_PE_ROLE_QUERY = 'select SCHOOL_PE_ROLE_ID, VALUE, SORT_ORDER, IS_DELETED, CREATED_AT, MODIFIED_AT from SCHOOL_PE_ROLE where MODIFIED_AT >=%s and MODIFIED_AT < %s'
    SCHOOL_TENNIS_UNIT_DURATION_QUERY = 'select SCHOOL_TENNIS_UNIT_DURATION_ID, VALUE, SORT_ORDER, IS_DELETED, CREATED_AT, MODIFIED_AT from SCHOOL_TENNIS_UNIT_DURATION where MODIFIED_AT >=%s and MODIFIED_AT < %s'
    SCHOOL_REGISTRY_QUERY = 'select SCHOOL_REGISTRY_ID, NAME, ADDRESS1, CITY,STATE,ZIPCODE, PHONE,IS_VALIDATED,CREATED_AT,MODIFIED_AT,  sr.SCHOOL_REGISTRY_STATUS_ID, srst.VALUE as "SCHOOL_REGISTRY_STATUS_NAME",  sr.SCHOOL_REGISTRY_TYPE_ID, srt.VALUE as "SCHOOL_REGISTRY_TYPE_NAME",  sr.SCHOOL_REGISTRY_SOURCE_ID, srso.VALUE as "SCHOOL_REGISTRY_SOURCE_NAME",  EXT_ID, IS_DELETED from SCHOOL_REGISTRY sr left join SCHOOL_REGISTRY_STATUS srst on sr.SCHOOL_REGISTRY_STATUS_ID = srst.SCHOOL_REGISTRY_STATUS_ID left join SCHOOL_REGISTRY_TYPE srt on sr.SCHOOL_REGISTRY_TYPE_ID = srt.SCHOOL_REGISTRY_TYPE_ID left join SCHOOL_REGISTRY_SOURCE srso on sr.SCHOOL_REGISTRY_SOURCE_ID = srso.SCHOOL_REGISTRY_SOURCE_ID where sr.MODIFIED_AT >=%s and sr.MODIFIED_AT < %s'
    SCHOOL_PROGRAM_SHIPPING_ADDRESS = 'select psa.PROGRAM_SHIPPING_ADDRESS_ID, EXT_ID from PROGRAM_SHIPPING_ADDRESS psa LEFT JOIN SCHOOL_PE_PROGRAM spp ON psa.PROGRAM_SHIPPING_ADDRESS_ID = spp.PROGRAM_SHIPPING_ADDRESS_ID WHERE spp.MODIFIED_AT >=%s and spp.MODIFIED_AT < %s'

    db=pymysql.connect(host=json_data['host'],port=3306,db='USTAPrograms',user=json_data['username'],passwd=json_data['password'])

    now = datetime.now()
    date_time = now.strftime("%Y%m%d_%H%M%S")
    endTime = now.strftime("%Y-%m-%d %H:%M:%S")
    SCHOOL_PE_PROGRAMS_FILENAME = 'usta_dl_ustaprograms_school_pe_program_' + date_time + '.csv'
    SCHOOL_PE_PROGRAM_SCHOOL_SEASONS_FILENAME = 'usta_dl_ustaprograms_school_pe_program_school_seasons_' + date_time + '.csv'
    SCHOOL_PE_ROLE_FILENAME = 'usta_dl_ustaprograms_school_pe_role_' + date_time + '.csv'
    SCHOOL_TENNIS_UNIT_DURATION_FILENAME = 'usta_dl_ustaprograms_school_tennis_unit_duration_' + date_time + '.csv'
    SCHOOL_REGISTRY_FILENAME = 'usta_dl_ustaprograms_school_registry_' + date_time + '.csv'
    SCHOOL_PROGRAM_SHIPPING_ADDRESS_FILE_NAME = 'usta_dl_ustaprograms_program_shipping_address_' + date_time + '.csv'

    createFile(dataQuery=SCHOOL_PE_PROGRAMS_QUERY, fileName=SCHOOL_PE_PROGRAMS_FILENAME,dateExtractionKey='SCHOOL_PE_PROGRAMS_LAST_EXECUTION_TIME',dbconn=db, endTime=endTime)
    createFile(dataQuery=SCHOOL_PE_PROGRAM_SCHOOL_SEASONS_QUERY, fileName=SCHOOL_PE_PROGRAM_SCHOOL_SEASONS_FILENAME,dateExtractionKey='SCHOOL_PE_PROGRAM_SCHOOL_SEASONS_LAST_EXECUTION_TIME',dbconn=db)
    createFile(dataQuery=SCHOOL_PE_ROLE_QUERY, fileName=SCHOOL_PE_ROLE_FILENAME,dateExtractionKey='SCHOOL_PE_ROLE_LAST_EXECUTION_TIME',dbconn=db)
    createFile(dataQuery=SCHOOL_TENNIS_UNIT_DURATION_QUERY, fileName=SCHOOL_TENNIS_UNIT_DURATION_FILENAME,dateExtractionKey='SCHOOL_TENNIS_UNIT_DURATION_LAST_EXECUTION_TIME',dbconn=db)
    createFile(dataQuery=SCHOOL_REGISTRY_QUERY, fileName=SCHOOL_REGISTRY_FILENAME,dateExtractionKey='SCHOOL_REGISTRY_LAST_EXECUTION_TIME',dbconn=db)
    createFile(dataQuery=SCHOOL_PROGRAM_SHIPPING_ADDRESS, fileName=SCHOOL_PROGRAM_SHIPPING_ADDRESS_FILE_NAME,dateExtractionKey='SCHOOL_PROGRAM_SHIPPING_ADDRESS_LAST_EXECUTION_TIME',dbconn=db, endTime=endTime)

def createFile(dataQuery, fileName, dateExtractionKey, dbconn, endTime) :

    BUCKET_NAME = os.environ['bucket_name']
    S3=boto3.session.Session().client('s3')
    lastExecutionTime = getLastExecutionTime(feedname=dateExtractionKey)

    cur = dbconn.cursor()
    cur.execute(dataQuery,(lastExecutionTime,endTime))


    rows = cur.fetchall()
    column_names = [i[0] for i in cur.description]
    fileNameStr = '/tmp/' + fileName
    fp = open(fileNameStr, 'w')
    myFile = csv.writer(fp, lineterminator = '\n')
    myFile.writerow(column_names)
    myFile.writerows(rows)
    fp.close()
    if os.path.isfile(fileNameStr):
        print('File exist')
        S3.upload_file(fileNameStr, BUCKET_NAME, 'incoming/' + fileName)
        updateLastExecutionTime(feedname=dateExtractionKey, lastExecutionTime=endTime)
    else:
        print ("File not exist")

    return;


def getLastExecutionTime(feedname) :
    lastExecutionTime = ""
    response = table.get_item(
        Key={
            'feedname': feedname
        }
    )

    if (response.get('Item') is not None) :
        lastExecutionTime = response.get('Item').get('lastexecutiontime')
    else:
        oldTime = date(2000, 1, 1)
        lastExecutionTime = oldTime.strftime("%Y-%m-%d %H:%M:%S")
        table.put_item(
            Item={
                'feedname': feedname,
                'lastexecutiontime' :  lastExecutionTime
            }
        )

    return lastExecutionTime;


def updateLastExecutionTime(feedname, lastExecutionTime) :
    response = table.update_item(
        Key={
            'feedname': feedname
        },
        UpdateExpression="set lastexecutiontime = :r",
        ExpressionAttributeValues={
            ':r': lastExecutionTime
        },
        ReturnValues="UPDATED_NEW"
    )
